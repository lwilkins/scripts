# Derivation info script
Small script to quickly inspect the contents of a derivation. Simply `source
setup.sh` and then to run script just give it the file you wish to inspect as
the argument.