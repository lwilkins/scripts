import sys

import ROOT as r

if __name__=="__main__":
    r.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
    file_name = sys.argv[1]
    if(not r.xAOD.Init().isSuccess()): print("Failed xAOD.Init()")

    root_file = r.TFile(file_name)
    tree = r.xAOD.MakeTransientTree(root_file)

    tree.Print()