import os, sys
import ROOT as r 
from optparse import OptionParser
import json
import cPickle as pickle

def write_output(catalogue, options):
    if options.pickle == "false":
        with open(options.output_name+".json", "w")as output_file:
            json.dump(catalogue, output_file, indent=2, sort_keys=True)
    else:
        with open(options.output_name+".pkl", "w") as output_file:
            pickle.dump(catalogue, output_file)

VERBOSE = True
if __name__ == "__main__":

    parser = OptionParser()

    parser.add_option("-d", "--directory", dest="directory", help="Directory to search through")
    parser.add_option("-o", "--ouput", dest="output_name", help="Output file name", default="root_catalogue")
    parser.add_option("-p", "--pickle", dest="pickle", help="Write output to pickle file instead", default="false")
    parser.add_option("-c", "--continue", dest="cont", help="Do you want to continue from where previous run left off", default="false")

    (options, args) = parser.parse_args()


    catalogue = { "files": []}
    n_root_files = 0
    
    if options.cont == "true":
        print(options.output_name + ".json")
        os.rename(options.output_name + ".json", options.output_name+"_old.json")
        with open(options.output_name+"_old.json", "r") as old_file:
            old_catalogue = json.load(old_file)
            catalogue = old_catalogue

    for subdir, dirs, files in os.walk(options.directory):
        for file in files:
            if file.endswith(".root"):
                file_path = subdir + os.sep + file
                file_path = os.path.realpath(file_path)
                if old_catalogue:
                    if file_path in old_catalogue:
                        continue
                if VERBOSE:
                    print("Checking file {0}".format(file_path))
            
                root_file = r.TFile(file_path, "READ")   
                temp_catalogue = {}
                try:
                    temp_catalogue["date_modified"] = os.path.getmtime(file_path)
                except OSError as e:
                    if os.path.islink(file_path):
                        print("Not a real symlink! Setting date_modified as 1.")
                        continue
                    else:
                        raise e
                temp_catalogue["file_path"] = file_path
                temp_catalogue["root_name"] = root_file.GetName()
                temp_catalogue["root_title"] = root_file.GetTitle()
                temp_catalogue["size"] = root_file.GetSize()
                if root_file.IsZombie():
                    temp_catalogue["is_zombie"] = 1
                else:
                    temp_catalogue["is_zombie"] = 0
                    keys = root_file.GetListOfKeys()
                    key_list = []
                    n_trees = 0
                    for key in keys:
                        key_dict = {}
                        key_dict["name"] = key.GetName()
                        key_dict["type"] = key.GetClassName()
                        key_list.append(key_dict)
                        if key_dict["type"] == "TTree":
                            tree_dict = {}
                            branches = []
                            tree = root_file.Get(key_dict["name"])
                            if tree:
                                branches = tree.GetListOfBranches()
                                branches = [branch.GetName() for branch in branches]
                                tree_dict["name"] = tree.GetName()
                                tree_dict["n_entries"] = tree.GetEntries()
                                tree_dict["branches"] = branches
                                key_dict["tree"] = tree_dict
                        
                    temp_catalogue["keys"] = key_list
                    
                catalogue["files"].append(temp_catalogue)
                root_file.Close()
                if n_root_files % 200 == 0:
                    write_output(catalogue, options)
                    print("Dumping to file")
                n_root_files+=1
    write_output(catalogue, options)