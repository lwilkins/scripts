import os, sys
import ROOT as r
import glob



def check_for_tree(file_path, tree_name):
    root_file = r.TFile(file_path)
    keys = root_file.GetListOfKeys()
    print(root_file.GetTitle())
    for key in keys:
        key_name = key.GetName()
        if key_name == tree_name:
            return True
        
    return False


if __name__ == "__main__":
    directory = sys.argv[1]
    tree_name = sys.argv[2]

    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith(".root"):
                file_path = subdir + os.sep + f
                
                if check_for_tree(file_path, tree_name):
                    with open("{0}_root_files.txt", "a") as output:
                        output.write(file_path)