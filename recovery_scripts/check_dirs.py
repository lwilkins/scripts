import os, sys, glob


if __name__ == "__main__":
    root_dir = "/recovered/lwilkins"
    in_file = sys.argv[1]
    ext = sys.argv[2]

    with open(in_file) as input_file:
        directories = input_file.readlines()
    directories = [x.strip() for x in directories]

    for dir in directories:
        if not "." in dir:
            dir_check = "{0}/{1}".format(root_dir, dir)
            print(dir_check)
            for root, dirs, files in os.walk(dir_check):
                for f in files:
                    if ext in f:
                        print(f)