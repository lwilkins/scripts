import os
import sys
from optparse import OptionParser
import os


if __name__ == "__main__":

    parser = OptionParser()

    parser.add_option("-i", "--input", dest="input_dir", help="Input directory containing samples")
    parser.add_option("-o", "--ouput", dest="output_dir", help="Output directory for input file lists", default="/scratch0/lwilkins/Scripts/get_inputs/inputs/")

    (options, args) = parser.parse_args()

    for subdir, dirs, files in os.walk(options.input_dir):
        dir_name = subdir.split("/")[-1] #Getting just the directory name
        if ".root" in dir_name: #Only want to look in directories which are samples
            print("Directory {0} has the following files:".format(dir_name))
            file_name = dir_name.split(".")[2]
            
            with open("{0}/{1}_inputs.txt".format(options.output_dir, file_name), 'w') as output_file:
                for file in files:
                    if ".root" in file:
                        output_file.write("{0}/{1}\n".format(subdir, file))

  