import ROOT as r
import os, sys

if __name__ == "__main__":
    file_name = sys.argv[1]
    observable = sys.argv[2]
    root_file = r.TFile(file_name)
    if not os.path.isdir(observable):
        os.mkdir(observable)
    keys = root_file.GetListOfKeys()

    for key in keys:
        key_name = key.GetName()
        if "regBin" in key_name or "orig" in key_name or "Shape" in key_name:
            continue
        hist = root_file.Get(key_name)
        out_file = r.TFile("{0}/{1}.root".format(observable,key_name),"RECREATE")
        out_file.cd()
        hist.Write()
        out_file.Close()
    
    root_file.Close()